package org.spiralpoint.example.boundary;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Header;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.ClassLoaderAsset;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.spiralpoint.example.JAXRSApplication;
import org.spiralpoint.example.control.ConfigurationPairs;
import org.spiralpoint.example.entity.ConfigurationPair;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.net.URL;

import static com.jayway.restassured.RestAssured.basic;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;

@RunWith(Arquillian.class)
public class ConfigurationPairsResourceIT {

    protected static final String APPLICATION_JSON = "application/json";
    protected static Header acceptJson = new Header("Accept", APPLICATION_JSON);

    @Before
    public void setUp() throws Exception {

        RestAssured.baseURI = "http://" + System.getProperty("rest.server", "localhost");
        RestAssured.basePath = "/resources/";
        RestAssured.authentication = basic("admin", "admin");
    }

    @ArquillianResource
    private URL base;

    @Deployment(testable = false)
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(new ClassLoaderAsset("test-shiro.ini"), ArchivePaths.create("shiro.ini"))
                .addAsLibraries(Maven.resolver().resolve("ca.eastlink.eng.utilities:web-security:0.3")
                        .withTransitivity().asFile())
                .addClasses(JAXRSApplication.class, ConfigurationPairsResource.class, ConfigurationPairs.class,
                        ConfigurationPair.class);
    }

    @PersistenceContext(unitName = "test")
    EntityManager em;


    @Test
    public void testGetAll() throws Exception {
        given().header(acceptJson).contentType(ContentType.JSON).expect().log().ifError().when()
                .get(base + "resources/configuration").then().assertThat().statusCode(equalTo(200));
    }

    @Test
    public void testPutConfigurationPair() throws Exception {
        given().header(acceptJson).contentType(ContentType.JSON).body(new ConfigurationPair("key2", "value2")).expect().log().all().when()
                .put(base + "resources/configuration").then().assertThat().statusCode(equalTo(201));
    }


    @Test
    public void testDeleteConfigurationPair() throws Exception {
        given().header(acceptJson).contentType(ContentType.JSON).expect().log().all().when()
                .delete(base + "resources/configuration/1").then().assertThat().statusCode(equalTo(200));
    }
}