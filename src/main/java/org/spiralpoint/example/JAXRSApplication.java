package org.spiralpoint.example;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by iheggie on 06/11/14.
 */
@ApplicationPath("resources")
public class JAXRSApplication extends Application {
}
