package org.spiralpoint.example.boundary;

import org.spiralpoint.example.control.ConfigurationPairs;
import org.spiralpoint.example.entity.ConfigurationPair;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;

/**
 * Created by iheggie on 06/11/14.
 */
@Path("configuration")
@Produces("application/json")
public class ConfigurationPairsResource {

    @Inject
    ConfigurationPairs configurationPairs;

    @GET
    public List<ConfigurationPair> getAll() {
        return configurationPairs.getAll();
    }

    @PUT
    @Consumes("application/json")
    public Response putConfigurationPair(ConfigurationPair pair, @Context UriInfo uriInfo) {
//        configurationPairs.putConfigPair(pair);
//        return Response.ok().build();
        Response response;
//        if (!configurationPairs.getForKey(pair.getCfg_key()).getCfg_value().equals("")) {
//            response = Response.noContent().build();
//        } else {
        URI uri = uriInfo.getAbsolutePathBuilder().build(pair.getCfg_key());
        response = Response.created(uri).build();
//        }
        configurationPairs.putConfigPair(pair);
        return response;

    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") int id) {
        configurationPairs.delete(id);
        return Response.ok().build();
    }
}
