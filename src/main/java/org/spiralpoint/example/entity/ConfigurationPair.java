package org.spiralpoint.example.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by iheggie on 06/11/14.
 */
@Data
@Entity
@Table(name = "configuration")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "configuration")
@RequiredArgsConstructor
@NamedQueries(value = {
        @NamedQuery(name = ConfigurationPair.findAll, query = "select c from ConfigurationPair c"),
        @NamedQuery(name = ConfigurationPair.findForKey, query ="select c from ConfigurationPair c where c.cfg_key = :key")
})
public class ConfigurationPair {

    private static final String PREFIX = "org.spiralpoint.example.entity.ConfigurationPair.";
    public static final String findAll = PREFIX + "all";
    public static final String findForKey = PREFIX + "forKey";

    @Id
    @GeneratedValue(generator = "configuration_id_seq", strategy = GenerationType.AUTO)
    @SequenceGenerator(name = "configuration_id_seq", sequenceName = "configuration_id_seq", allocationSize = 1)
    private long id;

    @Column(unique = true)
    private String cfg_key;
    private String cfg_value;
    private boolean active;

    public ConfigurationPair(String key, String value) {
        this.cfg_key = key;
        this.cfg_value = value;
        this.active = true;
    }
}
