package org.spiralpoint.example.control;

import org.spiralpoint.example.entity.ConfigurationPair;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by iheggie on 06/11/14.
 */
@Stateless
public class ConfigurationPairs {


    @PersistenceContext
    EntityManager em;

    public List<ConfigurationPair> getAll() {
        TypedQuery<ConfigurationPair> q = em.createNamedQuery(ConfigurationPair.findAll, ConfigurationPair.class);
        return q.getResultList();
    }

    public ConfigurationPair getForKey(String key) {
        TypedQuery<ConfigurationPair> q = em.createNamedQuery(ConfigurationPair.findForKey, ConfigurationPair.class);
        q.setParameter("key", key);
        return q.getSingleResult();
    }

    public ConfigurationPair getForId(int id) {
        TypedQuery<ConfigurationPair> q = em.createQuery("select c from ConfigurationPair c where c.id = :id",
                ConfigurationPair.class);
        q.setParameter("id", id);
        return q.getSingleResult();
    }

    public void putConfigPair(ConfigurationPair pair) {
        em.persist(pair);

    }

    public void delete(int id) {
        em.remove(getForId(id));
    }
}
